//
//  creditOrDebitInvoice.m
//  RMSTaxiDriver
//
//  Created by Rahul Sharma on 06/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import "creditOrDebitInvoice.h"
#import "NetworkHandler.h"
#import "Helper.h"
#import "UpdateBookingStatus.h"

@implementation creditOrDebitInvoice

//creditOrDebitInvoice.xib
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"creditOrDebitInvoice" owner:self options:nil] firstObject];
    return self;
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window
{
    _dictionaryDetails = [dict mutableCopy];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    [_amopuntReceivedTxtField becomeFirstResponder];
    NSNumber *mateFee =[NSNumber numberWithInteger:[dict[@"amount"] doubleValue]];
    _tripAmt = [NSString stringWithFormat:@"%f",[dict[@"amount"] doubleValue]];
    float number = [dict[@"amount"] floatValue];
    int rounded = roundf(number);
    _tripAmtLbl.text = [NSString stringWithFormat:@"%d",rounded];
     NSString *minimumBal =[[NSUserDefaults standardUserDefaults] objectForKey:@"balanceLimit"];
    _minimumBalance.text = [NSString stringWithFormat:@"%@%@",[Helper getCurrencyUnit],minimumBal];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;

    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished)
     {
     }];
}

- (IBAction)doneBtnClicked:(id)sender
{
    NSString * minimumAmt = [[NSUserDefaults standardUserDefaults] objectForKey:@"MininumAmtToCollect"];
    NSLog(@"minimum anount %@",minimumAmt);
    if ([_amopuntReceivedTxtField.text integerValue]>=[minimumAmt integerValue]) {
        [self sendrequesttoCompletedBooking];
            }
    else
    {
        NSString * textlbl = NSLocalizedString(@"You must minimum has to pay", @"You must minimum has to pay");
     [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[NSString stringWithFormat:@"%@ %@",textlbl,minimumAmt]];
    }
}

-(void)sendrequesttoCompletedBooking
{
    NSString *deviceid = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSString *sessionToken =    [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken];
    NSString * balanaceType;
    NSString * creditOrdebitAMt = [NSString stringWithFormat:@"%f",[_amopuntReceivedTxtField.text doubleValue]-[_tripAmt doubleValue]];
    NSString * tripamount =    [[NSUserDefaults standardUserDefaults] objectForKey:@"tripamount"];
    if (creditOrdebitAMt > 0) {
        balanaceType = @"0";
    }
    else
    {
        balanaceType = @"1";
    }
    NSDictionary * params = @{
                              @"ent_amount":_amopuntReceivedTxtField.text,//[NSString stringWithFormat:@"%ld",(long)creditOrdebitAMt],
                              @"ent_email":_dictionaryDetails[@"email"],
                              @"ent_balance_type":[NSString stringWithFormat:@"%@",balanaceType],
                              @"ent_appnt_id":_dictionaryDetails[@"bid"],
                              @"ent_sess_token":sessionToken,
                              @"ent_dev_id":deviceid,
                              @"ent_actual_amt":tripamount,
                              };
    NSLog(@"UpdateCustomerWallet params %@",params);
    NetworkHandler * handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"UpdateCustomerWallet" paramas:params onComplition:^(BOOL succeeded, NSDictionary *response)
    {
        if (succeeded)
        {
            NSLog(@"UpdateCustomerWallet response %@",response);
            if (!response)
            {
                return;
            }
            else if ([response[@"errFlag"] integerValue]== 0)
            {
                [self updateToPassengerForDriverState:kPubNubDriverReachedDestinationLoation withIteration:-1];
                UpdateBookingStatus * update = [UpdateBookingStatus sharedInstance];
                [update stopUpdatingStatus];
                [self hidePOPup];
                if (self.delegate && [self.delegate respondsToSelector:@selector(doneBtnClickedTag)])
                {
                    [_delegate doneBtnClickedTag];
                }
            }
        }
    }];
}
-(void)updateToPassengerForDriverState:(PubNubStreamAction)state withIteration:(int)iteration{
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.iterations = iteration;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
}
-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished)
    {
                         
                         [self removeFromSuperview];
                     }];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    _enteredAmt = textField.text;
////    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
////    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
////    [formatter setLocale:[NSLocale currentLocale]];
//    
//    NSNumber *mateFee =[NSNumber numberWithInteger:[_amopuntReceivedTxtField.text integerValue]];
//    
////    NSString *totalMat = [formatter stringFromNumber:mateFee];
//    _amopuntReceivedTxtField.text=mateFee;
// 
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField == _amopuntReceivedTxtField)
//    {
//        
//        _enteredAmt = textField.text;
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
//        [formatter setLocale:[NSLocale currentLocale]];
//        
//        NSNumber *mateFee =[NSNumber numberWithInteger:[_amopuntReceivedTxtField.text doubleValue]];
//        
//        NSString *totalMat = [formatter stringFromNumber:mateFee];
//        _amopuntReceivedTxtField.text=totalMat;
//    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

@end
