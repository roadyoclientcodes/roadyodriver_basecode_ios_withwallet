//
//  acceptRejectView.m
//  Roadyo
//
//  Created by Rahul Sharma on 22/04/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import "acceptRejectView.h"
#import <AVFoundation/AVFoundation.h>
#import "HomeViewController.h"


@implementation acceptRejectView
@synthesize bookingDate;
@synthesize BookingID;
@synthesize BookingTime;
@synthesize PickupLocationAddress;
@synthesize bookingIdLbl;
@synthesize pickUpAddressLbl;
@synthesize bookinhgDateLbl;
@synthesize timeLbl;
@synthesize progressView;
@synthesize timer;
@synthesize remainingCounts;
@synthesize timerLbl;
-(id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"acceptRejectView" owner:self options:nil] firstObject];
    
    [_acceptBtnOut setTitle:NSLocalizedString(@"ACCEPT", @"ACCEPT") forState:UIControlStateNormal];
    [_rejectBtnOut setTitle:NSLocalizedString(@"REJECT", @"REJECT") forState:UIControlStateNormal];
    _pickLab.text = NSLocalizedString(@"PICKUP LOCATION", @"PICKUP LOCATION");
    return self;
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window
{
    _timeLeft.text = @"Timer";
    
    self.frame = window.frame;
    [window addSubview:self];
    _expireTime = 30;
    
    [self startCoundownTimer];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    NSDateFormatter *df = [self serverformatter];
    
    df = [self formatter];
    
    NSString *book= NSLocalizedString(@"BOOKING ID:", @"BOOKING ID:");
    
    timeLbl.text = [self changeDateFormateToTime:dict[@"dt"]];
    bookingIdLbl.text = [NSString stringWithFormat:@"%@ %ld",book,[dict[@"bid"] integerValue]];
    pickUpAddressLbl.text = dict[@"adr2"];
    bookinhgDateLbl.text=  [self changeDateFormate:dict[@"dt"]];
    _customercurrentBal.text = [NSString stringWithFormat:@"Customer Current Balance %@",dict[@"balance"]];
    [_customercurrentBal sizeToFit];
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished)
     {
     }];
}

-(void)countDown {
    if (--remainingCounts == 0) {
        timerLbl.text = [NSString stringWithFormat:@"%d",remainingCounts];
        [timer invalidate];
    }
}
- (IBAction)acceptBtn:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didButtonSelectWithTag:)])
    {
        [_delegate didButtonSelectWithTag:2];
    }
    [self hidePOPup];
}
- (NSDateFormatter *)serverformatter
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}
- (NSDateFormatter *)formatter
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM HH:mm";
    });
    return formatter;
}
- (IBAction)rejectBtn:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didButtonSelectWithTag:)])
    {
        [_delegate didButtonSelectWithTag:3];
    }
    [self hidePOPup];
}
- (IBAction)tapGestureAction:(id)sender {
    
    [self hidePOPup];
}

-(void)hidePOPup{
    
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished)
     {
         [self removeFromSuperview];
     }];
}



/**
 *  Method to change the date formate
 *
 *  @param originalDate Origin date formate [2012-11-22 10:19:04]
 *
 *  @return The new formatted Date string [22-Nov-2012]
 */
- (NSString *)changeDateFormate:(NSString *)originalDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];//2012-11-22 10:19:04
    
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"dd/MMM/yyyy"];//22-Nov-2012
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    NSLog(@"Date in new format : %@", formattedDateString);
    return formattedDateString;
}
- (NSString *)changeDateFormateToTime:(NSString *)originalDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];//2012-11-22 10:19:04
    
    
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"hh:mm a"];//05:30 AM
    NSString *formattedTimeString = [dateFormatter stringFromDate:date];
    
    NSLog(@"Time in new format : %@", formattedTimeString);
    return formattedTimeString;
}
-(void)startCoundownTimer
{
    self.progressView.thicknessRatio=0.5;
    self.progressView.progress = 1;
    self.progressView.showText =YES;
    self.progressView.roundedHead = YES;
   self.progressView.textColor = [UIColor whiteColor];
    [progressView setProgressText:[NSString stringWithFormat:@"%.0f",_expireTime]];
    
    if (![timer isValid])
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
}

- (void)timerTick
{
    timerLbl.text = [NSString stringWithFormat:@"%f",_expireTime];
    float Newvalue1 = _expireTime/30 - (0.033333333);
    float Newvalue = _expireTime - 1;
    _timeLeft.text = [NSString stringWithFormat:@"%.0f Seconds", Newvalue];
    [progressView setProgressText:[NSString stringWithFormat:@"%.0f", Newvalue]];
    progressView.progress = Newvalue1;
    _expireTime = Newvalue;
    if (Newvalue <= 0.0) {
        [timer invalidate];
        
        [progressView removeFromSuperview];
        [self hidePOPup];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTimerExpire)])
        {
            [_delegate didTimerExpire];
        }
    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet == accept && buttonIndex == 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didButtonSelectWithTag:)])
        {
            [_delegate didButtonSelectWithTag:2];
        }
        
        [self hidePOPup];
    }
    else if (actionSheet == reject && buttonIndex == 0)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didButtonSelectWithTag:)])
        {
            [_delegate didButtonSelectWithTag:3];
        }
        [self hidePOPup];

    }
}
@end
