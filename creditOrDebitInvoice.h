//
//  creditOrDebitInvoice.h
//  RMSTaxiDriver
//
//  Created by Rahul Sharma on 06/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol creditOrDebitInvoice <NSObject>

-(void)doneBtnClickedTag;

@end

@interface creditOrDebitInvoice : UIView<UITextFieldDelegate>

@property (weak, nonatomic) id<creditOrDebitInvoice>delegate;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *amopuntReceivedTxtField;
@property (weak, nonatomic) IBOutlet UILabel *tripAmtLbl;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) NSString *enteredAmt;
@property (strong, nonatomic) NSString *tripAmt;
@property (strong, nonatomic) NSMutableDictionary *dictionaryDetails;

- (IBAction)doneBtnClicked:(id)sender;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
@property (weak, nonatomic) IBOutlet UILabel *minimumBalance;

@end
