//
//  GenericUtility.h
//  Homappy
//
//  Created by Vinay Raja on 07/12/13.
//  Copyright (c) 2013 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>



#define flStrForInt(x) [GenericUtility nonNullStringForInteger:x]
#define flStrForBool(x) [GenericUtility nonNullStringForBool:x]
#define flStrForDate(x) [GenericUtility nonNullStringForDate:x]
#define flStrForStr(x) [GenericUtility nonNullStringForString:x]
#define flStrForLong(x) [GenericUtility nonNullStringForLong:x]
#define flStrForDouble(x) [GenericUtility nonNullStringForDouble:x]
#define flIntForStr(x) [GenericUtility intFromString:x]
#define flBoolForStr(x) [GenericUtility boolFromString:x]
#define flDateForStr(x) [GenericUtility dateFromString:x]
#define flLongForStr(x) [GenericUtility longFromString:x]
#define flDoubleForStr(x) [GenericUtility doubleFromString:x]
#define flIntForObj(x) [GenericUtility intFromObject:x]
#define flBoolForObj(x) [GenericUtility boolFromObject:x]
#define flDateForObj(x) [GenericUtility dateFromObject:x]
#define flLongForObj(x) [GenericUtility longFromObject:x]
#define flDoubleForObj(x) [GenericUtility doubleFromObject:x]
#define flStrForObj(x) [GenericUtility stringFromObject:x]
#define flStrEqualsIgnoreCase(x,y) [GenericUtility isString1:x equalsIgnoreCaseToString2:y]
#define flHTMLEscapeStr(x) [GenericUtility stringByHTMLEscaping:x]

#define flNonEmptyString(str,default) str&&str.length>0?str:default

//Singleton to  provide the OS version check.
#define IS_OS_MAJOR_VERSION_LESS_THAN(x) ([GenericUtility DeviceSystemMajorVersion] < x)

#define startTiming(x) double x = [[NSDate date] timeIntervalSince1970];
#define endTiming(x) double x = [[NSDate date] timeIntervalSince1970];

@interface GenericUtility : NSObject


+ (NSUInteger)DeviceSystemMajorVersion;

//Conversion functions
+ (NSString *) nonNullStringForInteger:(NSInteger) value;
+ (NSString *) nonNullStringForBool:(NSInteger) value;
+ (NSString *) nonNullStringForDate:(NSDate *) date;
+ (NSString *) nonNullStringForString:(NSString *) string;
+ (NSString *) nonNullStringForLong:(long) string;
+ (NSString *) nonNullStringForDouble:(double) string;

+ (NSInteger) intFromString:(NSString *) string;
+ (NSDate *) dateFromString:(NSString *) string;
+ (BOOL) boolFromString:(NSString *) string;
+ (long) longFromString:(NSString *) string;
+ (double) doubleFromString:(NSString *) string;

+ (NSInteger) intFromObject:(id) obj;
+ (NSDate *) dateFromObject:(id) obj;
+ (BOOL) boolFromObject:(id) obj;
+ (long) longFromObject:(id) obj;
+ (double) doubleFromObject:(id) obj;
+ (NSString *) stringFromObject:(id) obj;

@end
