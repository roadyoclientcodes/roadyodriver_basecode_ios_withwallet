//
//  ShareCouponViewController.h
//  Tutree Tutor
//
//  Created by Rahul Sharma on 1/15/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareCouponViewController : UIViewController

- (IBAction)buttonFB:(id)sender;
- (IBAction)buttonTweet:(id)sender;
- (IBAction)buttonMsg:(id)sender;
- (IBAction)buttonEmail:(id)sender;
- (IBAction)buttonBack:(id)sender;

@end
