//
//  LocationTracker.m
//  Location
//
//  Created by Surender Rathore
//  Copyright (c) 2014 Location All rights reserved.
//

#import "LocationTracker.h"
#import "VNHPubNubWrapper.h"
#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"

@interface LocationTracker()<VNHPubNubWrapperDelegate>
@property(nonatomic,strong)NSString *pubNubChannel;

@end
static LocationTracker *locationtracker;
static int packedId = 0;
@implementation LocationTracker

+ (id)sharedInstance {
	if (!locationtracker) {
		locationtracker  = [[self alloc] init];
	}
	
	return locationtracker;
}


+ (CLLocationManager *)sharedLocationManager {
	static CLLocationManager *_locationManager;
	
	@synchronized(self) {
		if (_locationManager == nil) {
			_locationManager = [[CLLocationManager alloc] init];
           
            _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            
            if([_locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
                [_locationManager setAllowsBackgroundLocationUpdates:YES];
            }
		}
	}
	return _locationManager;
}

- (id)init {
	if (self==[super init]) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [LocationShareModel sharedModel];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	}
	return self;
}

-(void)applicationEnterBackground{
    
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;

    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates
{
    //NSLog(@"restartLocationUpdates");
    
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}


- (void)startLocationTracking {

	if ([CLLocationManager locationServicesEnabled] == NO) {
		UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	} else {
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        
        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
        }
        else
        {
            CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            [locationManager startUpdatingLocation];
        }
	}
}


- (void)stopLocationTracking {
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
	CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
	[locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate Methods
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    for(int i=0;i<locations.count;i++)
    {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        
        float distanceChange = [newLocation distanceFromLocation:_lastLocaiton];
        self.distance += distanceChange;
        if (self.distanceCallback) {
            self.distanceCallback(self.distance, newLocation);
        }
        
        if (self.distanceCallbackOnOff)
        {
            self.distanceCallbackOnOff(self.distanceOnOff,newLocation);
        }
         _lastLocaiton = newLocation;
        
        
        
        
        NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
        
        if (locationAge > 30.0)
        {
            continue;
        }
        //NSLog(@"the accuaracy %f",theAccuracy);
        
        //Select only valid location and also location with good accuracy
        if(newLocation!=nil && theAccuracy>0
           &&theAccuracy<2000
           &&(!(theLocation.latitude==0.0&&theLocation.longitude==0.0))){
            
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithFloat:theLocation.latitude] forKey:@"latitude"];
            [dict setObject:[NSNumber numberWithFloat:theLocation.longitude] forKey:@"longitude"];
            [dict setObject:[NSNumber numberWithFloat:theAccuracy] forKey:@"theAccuracy"];
            
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            [self.shareModel.myLocationArray addObject:dict];
        }
    }
    
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }
    
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}


//Stop the locationManager
-(void)stopLocationDelayBy10Seconds{
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
    
}
- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error
{
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please check your network connection." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case kCLErrorDenied:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enable Location Service" message:@"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //[alert show];
        }
            break;
        default:
        {
            
        }
            break;
    }
}


//Send the location to Server
- (void)updateLocationToServer
{
    // Find the best location from the array based on accuracy
    NSMutableDictionary * myBestLocation = [[NSMutableDictionary alloc]init];
    
    for(int i=0;i<self.shareModel.myLocationArray.count;i++){
        
        NSMutableDictionary * currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        
        if(i==0)
            myBestLocation = currentLocation;
        else
        {
            if([[currentLocation objectForKey:ACCURACY]floatValue]<=[[myBestLocation objectForKey:ACCURACY]floatValue]){
                myBestLocation = currentLocation;
            }
        }
    }
    //If the array is 0, get the last location
    //Sometimes due to network issue or unknown reason, you could not get the location during that  period, the best you can do is sending the last known location to the server
    if(self.shareModel.myLocationArray.count==0)
    {
        self.myLocation=self.myLastLocation;
        self.myLocationAccuracy=self.myLastLocationAccuracy;
    }
    else
    {
        CLLocationCoordinate2D theBestLocation;
        theBestLocation.latitude =[[myBestLocation objectForKey:LATITUDE]floatValue];
        theBestLocation.longitude =[[myBestLocation objectForKey:LONGITUDE]floatValue];
        self.myLocation=theBestLocation;
        self.myLocationAccuracy =[[myBestLocation objectForKey:ACCURACY]floatValue];
            [self startPubNubSream:self.myLocation.latitude Longitude:self.myLocation.longitude];
        //After sending the location to the server successful, remember to clear the current array with the following code. It is to make sure that you clear up old location in the array and add the new locations from locationManager
        [self.shareModel.myLocationArray removeAllObjects];
        self.shareModel.myLocationArray = nil;
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
    }
}
#pragma mark PUBNUB

-(void)startPubNubSream:(double)latitude Longitude:(double)longitude
{
    VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
    
    _pubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey];
    
    NSString *driverCardId = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverCarTypeIdKey];
    NSString *driverChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoSubscribeChanelKey];
    NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    NSString * bid=[[NSUserDefaults standardUserDefaults] objectForKey:@"bid"];
    NSString * profilePic = [[NSUserDefaults standardUserDefaults] objectForKey:@"pRofilePic"];
    NSString * carType = [[NSUserDefaults standardUserDefaults] objectForKey:@"LoginCarType"];
    NSString * driverProfileName = [[NSUserDefaults standardUserDefaults] objectForKey:@"DriverFirstNAme"];
    NSString * phoneNumb = [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
    NSString * driverid = [[NSUserDefaults standardUserDefaults] objectForKey:@"driverid"];
    NSString * cityId = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetcityId];
    if (bid == nil)
    {
        bid = @"0";
    }
    else
    {
        bid = bid;
    }
    
     NSDictionary *message =  @{
                     @"a":[[NSUserDefaults standardUserDefaults]objectForKey:@"currentBookingStatus"],//[NSNumber numberWithInt:kPubNubStartUploadLocation],
                     @"e_id": flStrForStr(driverEmail),
                     @"lt":[NSNumber numberWithDouble:latitude],
                     @"lg":[NSNumber numberWithDouble:longitude],
                     @"chn":flStrForStr(driverChannel),
                     @"id":[NSNumber numberWithInt:packedId],
                     @"bid":bid,
                     @"tp":flStrForStr(driverCardId),
                     @"carType":flStrForStr(carType),
                     @"profilePic":flStrForStr(profilePic),
                     @"fname":flStrForStr(driverProfileName),
                     @"phone":flStrForStr(phoneNumb),
                     @"driverid":flStrForStr(driverid),
                     @"cityid":flStrForStr(cityId)
                     };
    packedId++;
    NSLog(@"Updating to Server: %@",message);
    [pubNub publishWithParameter:message toChannel:_pubNubChannel];
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"currentBookingStatus"] intValue] != 4)
    {
        if ([[NSUserDefaults standardUserDefaults]  objectForKey:@"passengerDroppedPressed"])
        {
            NSDictionary *message1 =  @{
                                        @"a":@"100",
                                        @"e_id": driverEmail,
                                        @"lt":[NSNumber numberWithDouble:latitude],
                                        @"lg":[NSNumber numberWithDouble:longitude],
                                        @"chn":driverChannel,
                                        @"id":[NSNumber numberWithInt:packedId],
                                        @"bid":bid,
                                        @"tp":driverCardId,
                                        @"carType":carType,
                                        @"profilePic":flStrForStr(profilePic),
                                        @"fname":flStrForStr(driverProfileName),
                                        @"phone":flStrForStr(phoneNumb),
                                        @"driverid":flStrForStr(driverid),
                                        @"cityid":cityId
                                        };
            message = message1;
        }
        [pubNub getOccupancyCountOnDriverChannel:message onChannel:driverChannel];        
    }
}

#pragma mark - PubNubWrapperDelegate

-(void)recievedMessage:(NSDictionary*)messageDict onChannel:(NSString*)channelName;
{
    //NSLog(@"Message : %@", messageDict);
    if ([channelName isEqualToString:_pubNubChannel] ) {
    }
}
-(void)didFailedToConnectPubNub:(NSError *)error{
    
    // ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    
}
- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"MM-dd-yyyy hh:MM:ss";
    });
    return formatter;
}



@end
