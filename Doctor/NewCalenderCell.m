//
//  NewCalenderCell.m
//  Roadyo
//
//  Created by Rahul Sharma on 2/11/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "NewCalenderCell.h"

@implementation NewCalenderCell

- (void)awakeFromNib {
    _timeLabel.text = NSLocalizedString(@"TIME", @"TIME");
    _picLocLab.text = NSLocalizedString(@"PICKUP LOCATION:", @"PICKUP LOCATION:");
    _dropLocLab.text = NSLocalizedString(@"DROPOFF LOCATION:", @"DROPOFF LOCATION:");
    _bIdLabl.text = NSLocalizedString(@"BOOKING ID:", @"BOOKING ID:");
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedLanguage"] intValue] == 2)
    {
        
        _pickUpImage.frame = CGRectMake(self.frame.size.width - 30, 60, 15, 15);
        _dropOffImage.frame = CGRectMake(self.frame.size.width - 30, 146, 15, 15);
        _picLocLab.textAlignment = UITextAlignmentRight;
        _picLocLab.frame = CGRectMake(self.frame.size.width - 35 - 281, 57, 281, 21);
        _pickupLocation.textAlignment = UITextAlignmentRight;
        
        _dropLocLab.textAlignment = UITextAlignmentRight;
        _dropLocLab.frame = CGRectMake(self.frame.size.width - 35 - 283, 143, 281, 21);
        
        _dropoffLocation.textAlignment = UITextAlignmentRight;
        _passengerName.textAlignment = UITextAlignmentRight;
        _totalAmount.textAlignment = UITextAlignmentRight;
        _totalDistance.textAlignment = UITextAlignmentRight;
        _totalJourneyTime.textAlignment = UITextAlignmentRight;
    }
    

    
    // Initialization code
}
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//        
//        
//       
//    }
//    return self;
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
