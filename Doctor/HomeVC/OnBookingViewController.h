//
//  OnBookingViewController.h
//  Roadyo
//
//  Created by Rahul Sharma on 01/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"


@interface OnBookingViewController : UIViewController
{
    IBOutlet UILabel *lblNameTitle;
    IBOutlet UILabel *lblMobileTitle;
    IBOutlet UILabel *lblPickupTitle;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblMobile;
    IBOutlet UILabel *lblPickUp;
    IBOutlet UILabel *lblETATitle;
    IBOutlet UILabel *lblETA;
    IBOutlet UILabel *lblDistanceTitle;
    IBOutlet UILabel *lblDistance;
    IBOutlet UIButton *btnArrival;
    IBOutlet UIImageView *imgPass;
    IBOutlet UIView *viewPassDetail;
    IBOutlet UIView *viewStatus;
    IBOutlet UIButton *callButton;
    UIAlertView *driverStatus;
}
-(IBAction)buttonAction:(id)sender;
- (NSString *)changeDateFormateToTime:(NSString *)originalDate;
@property(strong,nonatomic)NSMutableDictionary *passDetail;
@property (weak, nonatomic) IBOutlet UILabel *BIDLbl;
@property (weak, nonatomic) IBOutlet UILabel *DateLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property(nonatomic,assign)BookingNotificationType bookingStatus;
@property(nonatomic,assign)int LoadFrom;
@property (weak, nonatomic) IBOutlet UILabel *DriverTxtLbl;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAndDropAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *cashOrCard;
@property (weak, nonatomic) IBOutlet UIImageView *CashImage;
@property (weak, nonatomic) IBOutlet UIButton *googleMapsBtn;
@property (weak, nonatomic) IBOutlet UIButton *wazeBtn;
@property (weak, nonatomic) IBOutlet UIView *AddessDetailsView;
@property (strong,nonatomic) LocationShareModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;

@end
