//
//  BookingNotificationView.h
//  Shiply
//
//  Created by Rathore on 20/10/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingNotificationView : UIView
-(void)updateBookingInformation:(NSDictionary*)bookingDetail bookingNotificaiton:(NSDictionary*)notification;
@end
