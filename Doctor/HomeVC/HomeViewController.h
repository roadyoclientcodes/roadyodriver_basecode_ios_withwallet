//
//  HomeViewController.h
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAPageIndicatorView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ViewPagerController.h"
#import "PICircularProgressView.h"

@interface HomeViewController : UIViewController<UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,UIGestureRecognizerDelegate,GMSMapViewDelegate>
{
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnReject;
    IBOutlet UILabel *labelPickupTitle;
    IBOutlet UILabel *labelDropOffTitle;
    IBOutlet UILabel *labelPickeup;
    IBOutlet UILabel *labelDropOff;
    IBOutlet UILabel *labelPickUpdistance;
    IBOutlet UILabel *labelDropOffdistance;
    IBOutlet UILabel *labelDropOffMile;
    IBOutlet UILabel *labelPickUpMile;
    IBOutlet UILabel *labelStatus;
    
    NSMutableDictionary * dictpassDetail;
    IBOutlet UIView *viewPushPopUp;
    ViewPagerController *viewPage;
    BOOL isPassangeBookDriver;
    
}
@property(nonatomic,assign)DriverStatus driverStatus;

@property(strong,nonatomic)NSDictionary * dictPush;
@property(strong,nonatomic)NSTimer * timer;
@property(assign,nonatomic)int counter;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;
@property(nonatomic,strong)NSMutableArray *laterBookings;

-(IBAction)doctorAvalabilityClicked:(id)sender;
- (IBAction)driverOfflineOnline:(UISwitch *)sender;
@property (strong, nonatomic) IBOutlet UISwitch *switchOnOff;
@property (strong, nonatomic) IBOutlet UILabel *availableLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dropIconImage;
@property (strong, nonatomic) IBOutlet UIImageView *dropLocationbackgroundImage;

@property (strong, nonatomic) IBOutlet UIButton *onTheJobButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *offTheJobButtonOutlet;
- (IBAction)offTheJobOnTheJobButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *todayTripsLab;
@property (strong, nonatomic) IBOutlet UILabel *todaysEarningLab;
@property (strong, nonatomic) IBOutlet UILabel *lastTripLab;
@property (strong, nonatomic) IBOutlet UILabel *estimatedNetLab;


@end
