//
//  NewBookingHistoryViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 2/7/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "NewBookingHistoryViewController.h"
#import "LocationTracker.h"
#import "MathController.h"
#import "UpdateBookingStatus.h"
#import "XDKAirMenuController.h"
#import "VNHPubNubWrapper.h"
#import "creditOrDebitInvoice.h"


@interface NewBookingHistoryViewController ()<creditOrDebitInvoice>
{
    NSString * apprAmount;
}
@property (strong, nonatomic) IBOutlet UILabel *systemCalcFare;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *waitingLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickLab;
@property (strong, nonatomic) IBOutlet UILabel *dropLab;
@property (strong, nonatomic) IBOutlet UIView *parkingLab;
@property (strong, nonatomic) IBOutlet UILabel *airportLab;
@property (strong, nonatomic) IBOutlet UILabel *tollLab;
@property (strong, nonatomic) IBOutlet UILabel *tipLab;
@property (strong, nonatomic) IBOutlet UILabel *totalLab;
@property (strong, nonatomic) IBOutlet UILabel *meterLab;
@property (strong, nonatomic) IBOutlet UILabel *parkLab;
@property (strong, nonatomic) IBOutlet UILabel *rateCust;

@property(nonatomic,assign) float rating;
@property (nonatomic, strong) UITextField *textFieldNew;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
@end

@implementation NewBookingHistoryViewController
{
    
}
@synthesize ratingStars,textFieldNew, numberFormatter;

- (void)viewDidLoad
{
    [super viewDidLoad];
    apprAmount = [[NSUserDefaults standardUserDefaults] objectForKey:@"apprAmount"];
    _systemCalcFare.text = NSLocalizedString(@"SYSTEM CALCULATED FARE", @"SYSTEM CALCULATED FARE");
    _distanceLabel.text = NSLocalizedString(@"DISTANCE", @"DISTANCE");
    _durationLabel.text = NSLocalizedString(@"DURATION", @"DURATION");
    _waitingLabel.text = NSLocalizedString(@"WAITING TIME", @"WAITING TIME");
    _pickLab.text = NSLocalizedString(@"PICKUP", @"PICKUP");
    _dropLab.text = NSLocalizedString(@"DROPOFF", @"DROPOFF");
    
    _meterLab.text = NSLocalizedString(@"METER", @"METER");
     _parkLab.text = NSLocalizedString(@"PARKING", @"PARKING");
    
    _airportLab.text = NSLocalizedString(@"AIRPORT", @"AIRPORT");
    _tollLab.text = NSLocalizedString(@"TOLL", @"TOLL");
    _tipLab.text = NSLocalizedString(@"TIP", @"TIP");
    _totalLab.text = NSLocalizedString(@"TOTAL", @"TOTAL");
    _rateCust.text = NSLocalizedString(@"RATE YOUR CUSTOMER", @"RATE YOUR CUSTOMER");
    
    _meterCharges.userInteractionEnabled = NO;
    _tipCharges.userInteractionEnabled = NO;
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedLanguage"] intValue] == 2)
    {
        _meterLab.textAlignment = NSTextAlignmentRight;
        _parkLab.textAlignment = NSTextAlignmentRight;
        _airportLab.textAlignment = NSTextAlignmentRight;
        _tollLab.textAlignment = NSTextAlignmentRight;
        _tipLab.textAlignment = NSTextAlignmentRight;
        _totalLab.textAlignment = NSTextAlignmentRight;
    }
    // Do any additional setup after loading the view.
    NSLog(@"PassDetails: %@", self.passDetails);
    self.title = NSLocalizedString(@"JOURNEY DETAILS", @"JOURNEY DETAILS");
    CGRect frame;
    frame = self.scrollView.frame;
    frame.size.height = self.view.frame.size.height;
    self.scrollView.frame = frame;
    [self sendRequestForAppointmentInvoice];
    [self currencyLabel];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
     self.navigationItem.hidesBackButton = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if ([_commingFromHistory isEqualToString:@"YES"])
    {
        self.navigationItem.hidesBackButton = NO;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", @"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(DoneButtonPressed:)];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:Robot_Regular size:15]} forState:UIControlStateNormal];
    }
    
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    [menu disablePanGesture:YES];
}
-(void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    
}
- (void)ratingChanged:(AXRatingView *)sender
{
    _rating = sender.value;
    
}
-(void)handleSingleTap:(UITapGestureRecognizer*)reconiger{
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:.4 animations:^{
        self.view.frame = frame;
    }];
    
    
    [self.view endEditing:YES];
}

-(IBAction)DoneButtonPressed:(id)sender
{
    [self sendRequestToFinishBooking];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(bookingComplete == alertView && buttonIndex == 0)
        [alertView removeFromSuperview];
        [self sendRequestToFinishBooking];
}

/**
 *  Get booking invoice
 */

-(void)sendRequestForAppointmentInvoice
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    
    NSString  *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *email = self.passDetails[@"email"];
    NSString *appointmentDate = self.passDetails[@"apptDt"];
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_email":email,
                             @"ent_user_type":@"1",
                             @"ent_appnt_dt":appointmentDate,
                             @"ent_date_time":currentDate,
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self parseAppointmentDetailResponse:response];
                                   }
                                   else {
                                       
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

-(void)sendRequestToFinishBooking
{
    if([apprAmount integerValue]>0)
    {
        creditOrDebitInvoice * creditView = [[creditOrDebitInvoice alloc] init];
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [creditView showPopUpWithDetailedDict:_passDetails Onwindow:window];
        creditView.delegate = self;
    }
    else
    {
        [self updateToPassengerForDriverState:kPubNubDriverReachedDestinationLoation];
        UpdateBookingStatus * update = [UpdateBookingStatus sharedInstance];
        [update stopUpdatingStatus];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveLatLongs"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"surge"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"bid"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklat"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklog"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerChannelKeyFromAPPTStatus];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self.navigationController popToRootViewControllerAnimated:YES];
    }

   /*
    ProgressIndicator * pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];

    NSString *email = self.passDetails[@"email"];
    NSString *appointmentDate = self.passDetails[@"apptDt"];
   NSString * cityId = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetcityId];
    NSDictionary *dictionary = @{
                                 @"ent_sess_token": [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_pas_email": email,
                                 @"ent_appnt_dt": appointmentDate,
                                 @"ent_response": constkNotificationTypeBookingComplete,
                                 @"ent_drop_addr_line1": self.passDetails[@"dropAddr1"],
                                 @"ent_amount": self.meterCharges.text,
                                 @"ent_meter": self.meterCharges.text,
                                 @"ent_toll": self.tollCharges.text,
                                 @"ent_airport": self.airportCharges.text,
                                 @"ent_parking": self.parkingCharges.text,
                                 @"ent_rating": [NSString stringWithFormat:@"%f", _rating],
                                 @"ent_cityid":cityId,
                                 @"ent_date_time": [Helper getCurrentDateTime]
                                 };
    NSLog(@"Finish Booking Parameters: %@", dictionary);
    NetworkHandler *networkHandler = [NetworkHandler sharedInstance];
    [networkHandler composeRequestWithMethod:@"updateApptStatus" paramas:dictionary onComplition:^(BOOL succeeded, NSDictionary *response)
     {
       [pi hideProgressIndicator];
        NSLog(@"Response Finish Booking: %@", response);
        if (succeeded)
        {
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"bid"];
            if ([response[@"errFlag"] integerValue] == 0) {
                
                [self updateToPassengerForDriverState:kPubNubDriverReachedDestinationLoation];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveLatLongs"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"surge"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"bid"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklat"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklog"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerChannelKeyFromAPPTStatus];
                [[NSUserDefaults standardUserDefaults] synchronize];
//                [self.navigationController popToRootViewControllerAnimated:YES];
                NSString * apprAmount = [[NSUserDefaults standardUserDefaults] objectForKey:@"apprAmount"];
                
                if([apprAmount integerValue]>0)
                {
                    creditOrDebitInvoice * creditView = [[creditOrDebitInvoice alloc] init];
                    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                    [creditView showPopUpWithDetailedDict:_passDetails Onwindow:window];
                    creditView.delegate = self;
                }
                else
                {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
            else if ([response[@"errNum"] integerValue] == 41)
            {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
            }
        }
    }];*/
}

-(void)doneBtnClickedTag
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"doneBtnClied");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveLatLongs"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"surge"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"bid"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklat"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklog"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerChannelKeyFromAPPTStatus];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)updateToPassengerForDriverState:(PubNubStreamAction)state{
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.iterations = 5;
    [updateBookingStatus updateToPassengerForDriverState];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [updateBookingStatus startUpdatingStatus];
}

#pragma mark- UITextfield Delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGRect frame = self.scrollView.frame;
    frame.size.height = self.view.frame.size.height - 216;
    [UIView animateWithDuration:0.4 animations:^{
        self.scrollView.frame = frame;
    }];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    float meterAmount = [self.meterCharges.text floatValue];
    float parkingAmount = [self.parkingCharges.text floatValue];
    float airportAmount = [self.airportCharges.text floatValue];
    float tollAmount = [self.tollCharges.text floatValue];
    float tipAmount = [self.tipCharges.text floatValue];
    float discount = [[NSUserDefaults standardUserDefaults]doubleForKey:@"discount"];
    self.totalChargesNew.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit], meterAmount+parkingAmount+airportAmount+tollAmount+tipAmount-discount
                            ];
    CGRect frame= self.scrollView.frame;
    frame.size.height = self.view.frame.size.height;
    [UIView animateWithDuration:0.4 animations:^{
        self.scrollView.frame = frame;
    }];
    [textField resignFirstResponder];
    [textField endEditing:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (!numberFormatter) {
        numberFormatter = [[NSNumberFormatter alloc]init];
        
    }
    NSNumber *test = [numberFormatter numberFromString:[textField.text stringByAppendingString: string]];  // in case we entered two decimals
    NSArray  *arrayOfString = [textField.text componentsSeparatedByString:@"."];
    if ([string length] == 0) {
        return YES;
    }

    if ([arrayOfString count] > 1) {
        if ([arrayOfString[1] length] == 2) {
            return NO;
        }
    }
    
    return (test != nil);
    
}

-(void)dismissKeyboard {
    [self.meterCharges resignFirstResponder];
    [self.parkingCharges resignFirstResponder];
    [self.airportCharges resignFirstResponder];
    [self.tollCharges resignFirstResponder];
    [self.tipCharges resignFirstResponder];
    //[self.totalCharges resignFirstResponder];
    
}
-(void) currencyLabel{
    self.currencyLabel1.text = self.currencyLabel2.text = self.currencyLabel3.text = self.currencyLabel4.text = self.currentLabel5.text = [Helper getCurrencyUnit];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[AXRatingView class]])
    {
        [self ratingChanged:(AXRatingView *)touch.view];
        return NO;
    }else
    return YES;
}


/**
 *  Updating the invoice
 */
-(void)updateScreen {
    int timeHr,timeMin,timeSec;
    if ([self.passDetails[@"statCode"] integerValue] == 9) {
        self.labelDistance.text = [NSString stringWithFormat:@"%.2f %@", [self.passDetails[@"dis"] floatValue]*1.60834, [Helper getDistanceUnit]];
        long duration = [self.passDetails[@"dur"] integerValue];
        if (duration > 60) {
            timeHr = duration/60;
            timeMin = duration%60;
        }else
        {
            timeMin = duration;
            timeHr = 0;
        }
        self.labelDuration.text = [NSString stringWithFormat:@"%02dH : %02dM", timeHr, timeMin];
        
        duration = [self.passDetails[@"waitTime"] integerValue];
        if (duration > 60) {
            timeHr = duration/60;
            timeMin = duration%60;
        }else
        {
            timeMin = duration;
            timeHr = 0;
        }
        self.labelWaitingTime.text = [NSString stringWithFormat:@"%02dH : %02dM", timeHr, timeMin];

        
        self.parkingCharges.text = [NSString stringWithFormat:@"%.02f", [self.passDetails[@"parkingFee"] floatValue]];
        self.airportCharges.text = [NSString stringWithFormat:@"%.02f", [self.passDetails[@"airportFee"] floatValue]];
        self.tollCharges.text = [NSString stringWithFormat:@"%.02f", [self.passDetails[@"tollFee"] floatValue]];
        
    }else {
        float distance = [[LocationTracker sharedInstance] distance];
        [self.labelDistance setText:[[MathController stringifyDistance:distance] uppercaseString]];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSTimeInterval differenceBetweenStartAndCurrentTime = [[ud objectForKey:@"passengerDroppedPressed"] timeIntervalSinceDate:[ud objectForKey:@"beginTripPressed"]];
        int differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
        
        if (differenceBetweenStartAndCurrentTimeINT > 60) {
            timeSec = differenceBetweenStartAndCurrentTimeINT % 60;
            timeMin = differenceBetweenStartAndCurrentTimeINT/60;
            if (timeMin > 60) {
                timeHr = timeMin/60;
                timeMin = timeMin%60;
            }else{
                timeHr = 0;
            }
        }else{
            timeSec = differenceBetweenStartAndCurrentTimeINT;
            timeMin = 0;
            timeHr = 0;
        }
        [self.labelDuration setText:[NSString stringWithFormat:@"%02dH : %02dM", timeHr, timeMin]];
        differenceBetweenStartAndCurrentTime = [[ud objectForKey:@"beginTripPressed"]timeIntervalSinceDate:[ud objectForKey:@"iHaveArrivedPressed"]];
        differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
    
        if (differenceBetweenStartAndCurrentTimeINT > 60) {
            timeSec = differenceBetweenStartAndCurrentTimeINT % 60;
            timeMin = differenceBetweenStartAndCurrentTimeINT/60;
            if (timeMin > 60) {
                timeHr = timeMin/60;
                timeMin = timeMin%60;
            }else{
                timeHr = 0;
            }
        }else{
            timeSec = differenceBetweenStartAndCurrentTimeINT;
            timeMin = 0;
            timeHr = 0;
        }
    
        [self.labelWaitingTime setText:[NSString stringWithFormat:@"%02dH : %02dM",timeHr, timeMin]];
    }
    
    NSString *stringPickupDate = self.passDetails[@"pickupDt"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"hh:mm a"];
    NSDate *pickupDate = [dateFormat dateFromString:stringPickupDate];
    [self.labelPickupTime setText:[[df stringFromDate:pickupDate] uppercaseString]];
    NSDate *dropOffDate;
    if ([self.passDetails[@"statCode"] integerValue] == 9) {
        NSString *stringDropDate = self.passDetails[@"dropDt"];
        dropOffDate = [dateFormat dateFromString:stringDropDate];
    }else{
        dropOffDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"passengerDroppedPressed"];
    }
    
    
    [self.labelDropOffTime setText:[[df stringFromDate:dropOffDate] uppercaseString]];
    
    [self.labelPickupAddress setText:[NSString stringWithFormat:@"%@ %@",self.passDetails[@"addr1"], self.passDetails[@"addr2"]]];
    CGRect labelSize = [self.labelPickupAddress.text boundingRectWithSize:CGSizeMake(self.labelPickupAddress.frame.size.width, self.view.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:14]} context:nil];
    
    CGRect frame = self.labelPickupAddress.frame;
    frame.size.height = labelSize.size.height;
    self.labelPickupAddress.frame = frame;
    
    frame = self.viewPickupAddress.frame;
    frame.size.height = self.labelPickupAddress.frame.size.height + 5;
    self.viewPickupAddress.frame = frame;
    
    frame = self.viewPickup.frame;
    frame.size.height = self.viewPickupAddress.frame.origin.y + self.viewPickupAddress.frame.size.height;
    self.viewPickup.frame = frame;
    
    [self.labelDropOffAddress setText:[NSString stringWithFormat:@"%@ %@", self.passDetails[@"dropAddr1"], self.passDetails[@"dropAddr2"]]];
    labelSize = [self.labelDropOffAddress.text boundingRectWithSize:CGSizeMake(self.labelDropOffAddress.frame.size.width, self.view.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:14]} context:nil];
    frame = self.labelDropOffAddress.frame;
    frame.size.height = labelSize.size.height;
    self.labelDropOffAddress.frame = frame;
    
    frame = self.viewDropOffAddress.frame;
    frame.size.height = self.labelDropOffAddress.frame.size.height + 5;
    self.viewDropOffAddress.frame = frame;
    
    frame = self.viewDropOff.frame;
    frame.origin.y = self.viewPickup.frame.origin.y + self.viewPickup.frame.size.height;
    frame.size.height = self.viewDropOffAddress.frame.origin.y + self.viewDropOffAddress.frame.size.height;
    self.viewDropOff.frame = frame;
    
    frame = self.viewFareDetails.frame;
    frame.origin.y = self.viewDropOff.frame.origin.y + self.viewDropOff.frame.size.height;
    self.viewFareDetails.frame = frame;
    
    frame = self.viewRateYourCustomer.frame;
    frame.origin.y = self.viewFareDetails.frame.origin.y + self.viewFareDetails.frame.size.height;
    self.viewRateYourCustomer.frame = frame;
    
    _rating = 0.0;
    
    //_ratingViewStars = [[AXRatingView alloc] initWithFrame:CGRectMake(0, 0,self.ratingView.frame.size.width, self.ratingView.frame.size.height)];
    // _ratingViewStars.markImage = [UIImage imageNamed:@"Icon"];
    ratingStars.stepInterval = 0.0;
    ratingStars.baseColor = [UIColor colorWithRed:0.293 green:0.303 blue:0.322 alpha:1.000];
    ratingStars.highlightColor =  [UIColor colorWithRed:0.975 green:0.736 blue:0.056 alpha:1.000];
    ratingStars.value = _rating;
    // ratingStars.markFont = [UIFont systemFontOfSize:10];
    ratingStars.userInteractionEnabled = YES;
    [ratingStars addTarget:self action:@selector(ratingChanged:) forControlEvents:UIControlEventValueChanged];
    if ([self.passDetails[@"statCode"] integerValue] == 9) {
        [self.viewRateYourCustomer setHidden:YES];
        [self.meterCharges setEnabled:NO];
        [self.airportCharges setEnabled:NO];
        [self.parkingCharges setEnabled:NO];
        [self.tollCharges setEnabled:NO];
    }else {
        
        [self.viewRateYourCustomer setHidden:NO];
        [self.meterCharges setEnabled:YES];
        [self.airportCharges setEnabled:YES];
        [self.parkingCharges setEnabled:YES];
        [self.tollCharges setEnabled:YES];
    }
    //[_ratingViewStars sizeToFit];
    //[_ratingView addSubview:_ratingViewStars];
    
    //    UITapGestureRecognizer *reconiger = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //    //reconiger.numberOfTapsRequired = 1;
    //    [self.view addGestureRecognizer:reconiger];
    if ([self.passDetails[@"statCode"] integerValue] == 9) {
        self.scrollView.contentSize = CGSizeMake(320, self.viewFareDetails.frame.origin.y + self.viewFareDetails.frame.size.height);
        
        
        
    }else {
        self.scrollView.contentSize = CGSizeMake(320, self.viewRateYourCustomer.frame.origin.y + self.viewRateYourCustomer.frame.size.height);
    }
    float discount = [[NSUserDefaults standardUserDefaults]doubleForKey:@"discount"];
    self.totalChargesNew.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit], [self.meterCharges.text floatValue]+[self.tipCharges.text floatValue]-discount
                                 ];
    
}

-(void)parseAppointmentDetailResponse:(NSDictionary*)response
{
    NSLog(@"Response Invoice: %@", response);
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
        response  =    response[@"data"];
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [self.systemCalculatedFare setText:[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[apprAmount doubleValue]]];
            [self.surgePriceLbl setText:[NSString stringWithFormat:@"SURGE:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"surge"]]];
            [[NSUserDefaults standardUserDefaults]setDouble:[response[@"discount"] doubleValue] forKey:@"discount"];
            self.passDetails = (NSMutableDictionary *)response;
            double tip = [[NSUserDefaults standardUserDefaults] doubleForKey:@"tip"];
            if ([self.passDetails[@"statCode"] integerValue] == 8)
            {
                double totalAmtValue = [response[@"airportFee"] doubleValue] + [response[@"meterFee"] doubleValue] + [response[@"parkingFee"] doubleValue] + [response[@"tollFee"] doubleValue] - [response[@"discount"] doubleValue];
                self.totalCharges.text = [NSString stringWithFormat:@" %.02f", totalAmtValue];
                double totalTipAmtValue = totalAmtValue + tip;
                self.totalNewPrice.text = [NSString stringWithFormat:@"%.02f", totalTipAmtValue];
                
                self.meterCharges.text = [NSString stringWithFormat:@"%.02f", [apprAmount doubleValue]];
                self.tipCharges.text = [NSString stringWithFormat:@"%.02f", [response[@"tip"] doubleValue]];
            }
            else
            {
                self.meterCharges.text = [NSString stringWithFormat:@"%.02f", [response[@"meterFee"] doubleValue]];
                self.totalCharges.text = [NSString stringWithFormat:@" %.02f", [response[@"amount"] doubleValue]-[response[@"tip"] doubleValue]-[response[@"discount"] doubleValue]];
                self.totalNewPrice.text = [NSString stringWithFormat:@" %.02f", [response[@"amount"] doubleValue]];
                self.tipCharges.text = [NSString stringWithFormat:@"%.02f", [response[@"tip"] doubleValue]];
            }
            [self updateScreen];
        }
    }
}



@end
