 //
//  CheckBookingStatus.m
//  Roadyo
//
//  Created by Surender Rathore on 27/06/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "CheckBookingStatus.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"

@implementation CheckBookingStatus
@synthesize callblock;
static CheckBookingStatus  *bookingStatus;

+ (id)sharedInstance
{
	if (!bookingStatus)
    {
		bookingStatus  = [[self alloc] init];
	}
	return bookingStatus;
}


-(void)checkOngoingAppointmentStatus:(NSString *)appDate {
    
   
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *appointmntDate; //= appDate;
    if (appDate == nil) {
        appointmntDate = @"";
    }

    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":flStrForStr(sessionToken),
                             @"ent_dev_id":deviceID,
                             @"ent_user_type":@"1",
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    
    NSLog(@"kSMGetAppointmentDetial %@",params);
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    
    //[networHandler cancelRequest];
    [networHandler composeRequestWithMethod:@"getApptStatus"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   NSLog(@"getattpstatus response>>>>>>>>>>>>>>>>>>>>%@",response);
                                   if (success) { //handle success response
                                       if ([response[@"errFlag"] integerValue] == 1) {
                                            NSString * lastTripTime =[self changeDateFormateToTime:response[@"lastTripTime"]];
                                           [[NSUserDefaults standardUserDefaults]setObject:response[@"wallet"] forKey:@"walletAmount"];
                                           [[NSUserDefaults standardUserDefaults]setObject:response[@"tripsToday"] forKey:@"tripsToday"];
                                           [[NSUserDefaults standardUserDefaults]setObject:response[@"earningsToday"] forKey:@"earningstoday"];
                                           [[NSUserDefaults standardUserDefaults]setObject:response[@"lastTripPrice"] forKey:@"lastTripPrice"];
                                           [[NSUserDefaults standardUserDefaults]setObject:lastTripTime forKey:@"lastTripTime"];//balanceLimit
                                           [[NSUserDefaults standardUserDefaults]setObject:response[@"balanceLimit"] forKey:@"balanceLimit"];
                                           [[NSUserDefaults standardUserDefaults]synchronize];
                                       }
                                       else
                                       {
                                           
                                       }
                                       [self parsepollingResponse:response];
                                   }
                                   else{
                                   }
                               }];
}

-(void)parsepollingResponse:(NSDictionary *)responseDict
{
    if (responseDict == nil)
    {
        if(self.callblock)
            self.callblock(0,responseDict);
        return;
    }
    else if ([responseDict objectForKey:@"error"])
    {
        if(self.callblock)
            self.callblock(0,responseDict);
    }
    else
    {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 0)
        {
            int status = 0;
            NSDictionary *data = [responseDict[@"data"][0] mutableCopy];
            if (data)
            {
                status = [data[@"status"] integerValue];
            }
            if ([[responseDict allKeys] containsObject:@"data"])
            {
                responseDict = [responseDict[@"data"][0] mutableCopy];
            }
            self.callblock(status,responseDict);
        }
        else
        {
            if(self.callblock)
                self.callblock(0,responseDict);
        }
    }
}

//-(void)parsepollingResponse:(NSDictionary *)responseDict
//{
//    if (responseDict == nil)
//    {
//        self.callblock(0,nil);
//        return;
//    }
//    else if ([responseDict objectForKey:@"error"])
//    {
//       // [Helper showAlertWithTitle:@"Error" Message:[responseDict objectForKey:@"error"]];
//        self.callblock(0,nil);
//    }
//    else
//    {
//        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 0)
//        {
//            NSDictionary *response = responseDict[@"data"][0];
//            int status = [response[@"status"] intValue];
//                self.callblock(status,response);
//        }
//        else
//        {
//            self.callblock(0,nil);
//        }
//    }
//}
- (NSString *)changeDateFormateToTime:(NSString *)originalDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2012-11-22 10:19:04
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"hh:mm a"];//05:30 AM
    NSString *formattedTimeString = [dateFormatter stringFromDate:date];
    
    NSLog(@"Time in new format : %@", formattedTimeString);
    return formattedTimeString;
}



@end
