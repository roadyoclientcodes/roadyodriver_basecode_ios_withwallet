//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "Helper.h"
#import "Fonts.h"
#import "LocationServicesViewController.h"
#import "Locale.h"
#import "LanguageManager.h"
#import "NSBundle+ForceLocalization.h"

//#import <Canvas/CSAnimationView.h>

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize bottomContainerView;
@synthesize signInBtn;
@synthesize registerBtn;
@synthesize mainScrollView;
@synthesize pageControl;
@synthesize orLbl;
@synthesize langaugeChangeBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self createScrollingView];
    [self.view bringSubviewToFront:bottomContainerView];
    [self.view bringSubviewToFront:pageControl];
    [self.view bringSubviewToFront:langaugeChangeBtn];
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    Locale *localeObj;
    NSString *str;
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"ar"])
    {
        localeObj = languageManager.availableLocales[0];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        str = @"العربية";
        [self.langaugeChangeBtn setSelected:NO];
    }
    else
    {
        localeObj = languageManager.availableLocales[1];
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        str = @"ENGLISH";
        [self.langaugeChangeBtn setSelected:YES];
    }
    [languageManager setLanguageWithLocale:localeObj];
    
    [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Robot_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    
    bottomContainerView.frame = CGRectMake(0,CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.frame),65);
    langaugeChangeBtn.frame = CGRectMake(20, CGRectGetHeight(self.view.frame)-CGRectGetHeight(bottomContainerView.frame) - 40, 280, 30);
    bottomContainerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"roadyo_footer.png"]];
    
    [self.view bringSubviewToFront:langaugeChangeBtn];
    [self.view bringSubviewToFront:bottomContainerView];
    
    [Helper setButton:signInBtn Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    signInBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [signInBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    signInBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [signInBtn setBackgroundImage:[UIImage imageNamed:@"123register_on@2x.png"] forState:UIControlStateHighlighted];
    
    
    [Helper setButton:registerBtn Text:NSLocalizedString(@"REGISTER", @"REGISTER") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [registerBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    registerBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"123register_on@2x.png"] forState:UIControlStateHighlighted];
    [self.view bringSubviewToFront:_swpLabel];
    [Helper setToLabel:_swpLabel Text:NSLocalizedString(@"swipe up", @"swipe up") WithFont:Robot_Regular FSize:14 Color:UIColorFromRGB(0xffffff)];
}
- (IBAction)langugaeChange:(id)sender
{
    if (self.langaugeChangeBtn.isSelected)
    {
        [self.langaugeChangeBtn setSelected:NO];
        NSString *str = @"العربى";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Robot_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[0];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
    else
    {
        [self.langaugeChangeBtn setSelected:YES];
        //        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-568h2"]];
        NSString *str = @"ENGLISH";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Robot_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[1];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
}
-(void) updateUI
{
    [Helper setButton:signInBtn Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [Helper setButton:registerBtn Text:NSLocalizedString(@"REGISTER", @"REGISTER") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    
    orLbl.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"roadyo_btn_or_on.png"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
}

-(void)viewDidAppear:(BOOL)animated;
{
    [self startAnimationUp];
    
}

-(void)startAnimationDown
{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         CGRect frame = bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height;
         frame.origin.x = 0;
         bottomContainerView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
-(void)startAnimationUp
{
    
    bottomContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, 320, 65);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect frame = bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height-65;
         frame.origin.x = 0;
         bottomContainerView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}


#pragma mark-scrollView delegate

//- (void)scrollViewDidScroll:(UIScrollView *)sender
//{
//    CGFloat pageHeight = self.mainScrollView.frame.size.height;
//    // you need to have a **iVar** with getter for scrollView
//
//    float fractionalPage = self.mainScrollView.contentOffset.y / pageHeight;
//    NSInteger page = lround(fractionalPage);
//    self.pageControl.currentPage = page;
//}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInButtonClicked:(id)sender {
    [self startAnimationDown];
    [self performSegueWithIdentifier:@"SignIn" sender:self];
}

//- (IBAction)pageControllerButtonClicked:(id)sender {
//    NSLog(@"pageControl position %ld", (long)[self.pageControl currentPage]);
//    //Call to remove the current view off to the left
//    unsigned long int page = self.pageControl.currentPage;
//
//    CGRect frame = mainScrollView.frame;
//    frame.origin.x = 0;
//    frame.origin.y = frame.size.width * page;
//    [mainScrollView scrollRectToVisible:frame animated:YES];
//}


- (IBAction)registerButtonClicked:(id)sender {
    [self startAnimationDown];
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}


/*
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 if ([[segue identifier] isEqualToString:@"SignIn"])
 {
 SignInViewController *SVC = segue.destinationViewController;
 
 SVC =[segue destinationViewController];
 
 }
 else
 {
 SignUpViewController *SVC = segue.destinationViewController;
 
 SVC =[segue destinationViewController];
 }
 
 }
 */
-(void)callPush
{
    // LoginViewController *LVC=[[LoginViewController alloc] init];
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:1.0];
    //  [self.navigationController pushViewController:LVC animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
}
//   Call Pop

-(void)callPop
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}

@end
