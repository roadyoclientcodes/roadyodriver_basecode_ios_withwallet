//
//  LaterBookingCell.h
//  Shiply
//
//  Created by Rathore on 01/10/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaterBookingCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *labelDate;
@property(nonatomic,weak)IBOutlet UILabel *labelName;
@property(nonatomic,weak)IBOutlet UILabel *labelAddressPickup;
@property(nonatomic,weak)IBOutlet UILabel *labelAddressDrop;
@property(nonatomic,weak)IBOutlet UILabel *labelBookingStatus;
@property(nonatomic,weak)IBOutlet UILabel *labelBookingId;
@property(nonatomic,weak)IBOutlet UIButton *buttonPhoneNuber;
@property(nonatomic,weak)IBOutlet UIButton *buttonCancel;
@property(nonatomic,weak)IBOutlet UIButton *buttonOnTheWay;
@property(nonatomic,weak)IBOutlet UIImageView *profileImageView;
@end
