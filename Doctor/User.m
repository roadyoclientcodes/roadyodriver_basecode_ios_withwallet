//
//  User.m
//  privMD
//
//  Created by Surender Rathore on 19/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "User.h"
//#import "Database.h"
#import "PMDReachabilityWrapper.h"
#import "NetworkHandler.h"
#import "VNHPubNubWrapper.h"

@implementation User
@synthesize delegate;
@synthesize blurredImage;
@synthesize profileImage;
@synthesize name;
@synthesize email;
@synthesize phone;


static User  *user;

+ (id)sharedInstance {
	if (!user) {
		user  = [[self alloc] init];
	}
	
	return user;
}


- (void)logout
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {

   NetworkHandler * handler = [NetworkHandler sharedInstance];
   NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [NSNumber numberWithInt:1],kSMPPassengerUserType,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    NSLog(@"param%@",queryParams);
    [handler composeRequestWithMethod:MethodLogout
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
    
                                if (succeeded) { //handle success response
                                      [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastBID"];
                                      [self userLogoutResponse:response];
                                  }
                                  else{
                                  }
                              }];
    }
    else
    {
    }
}

- (void) userLogoutResponse:(NSDictionary *)response
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    if ([response[@"errFlag"] intValue] ==0) {
        
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
        }
    }
    else if ([response[@"errFlag"] intValue] ==1)
    {
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
        }
    }
    else
    {
        [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
    }
}

-(void)deleteUserSavedData{
    
    //delete all saved cards
    //[Database deleteAllCard];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    VNHPubNubWrapper * pubnub = [VNHPubNubWrapper sharedInstance];
    [pubnub unsubscribeFromMyChannel];
    [pubnub unsubscribeFromAllChannel];
}
@end
