//
//  SignUpViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    
    kTagFirstName = 200,
    kTagLastName = 201,
    kTagEmail = 202,
    kTagMobileNumber = 203,
    kTagPasscode = 204,
    kTagTaxNumber = 205,
    kTagCompneyID = 206,
    kTagCompneyName = 207,
    kTagPassword = 208,
    kTagConfirmPassword = 209
    
}SignUpFormTags;

typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;

@interface SignUpViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *array;
    BOOL checkSignupCredentials;
    BOOL isTnCButtonSelected;
    UIPickerView *pickerview;
    UIActionSheet *newSheet;
    IBOutlet UIView * viewCarType;
    IBOutlet UIView * ViewSeatingCapacity;
    IBOutlet UILabel * lblCarType;
    IBOutlet UILabel * lblSeatingCapacity;
    
}

- (IBAction)signup:(id)sender;

@property(nonatomic, strong) NSMutableArray *helperCountry;
@property(nonatomic, strong) NSMutableArray *helperCity;

@property (strong, nonatomic) UIImage *pickedImage;

@property (strong, nonatomic) NSArray *saveSignUpDetails;

@property (weak, nonatomic) IBOutlet UIButton *termsLbl;

@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTF;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTF;
- (IBAction)selectImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;

- (IBAction)termsNConditions:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;

- (IBAction)checkBoxAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *selectCity;
@property (strong, nonatomic) IBOutlet UITextField *passTF;
@property (weak, nonatomic) IBOutlet UIImageView *countryFlag;
@property (weak, nonatomic) IBOutlet UIButton *countryCode;
@property (weak, nonatomic) IBOutlet UIButton *countryName;

@property (strong, nonatomic) IBOutlet UIButton *checkBox;
@property (strong, nonatomic) IBOutlet UIButton *selectVehicle;
- (IBAction)selectVehicleAction:(id)sender;

- (IBAction)selectCityAction:(id)sender;
@end
