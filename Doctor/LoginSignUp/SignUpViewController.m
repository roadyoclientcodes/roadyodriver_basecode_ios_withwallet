//
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
//#import "MapViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "HomeViewController.h"
#import "PMDReachabilityWrapper.h"
#import "DoneCancelNumberPadToolbar.h"
#import "Cartype.h"
#import "MenuViewController.h"
#import "Errorhandler.h"
#import "ResKitWebService.h"
#import "Fonts.h"
#import "Helper.h"
#import "ProgressIndicator.h"
#import "CustomNavigationBar.h"
#import "SignUp.h"
#import "UploadFiles.h"
#import "SignUpDetails.h"

#import "CountryPicker.h"
#import "CountryNameTableViewController.h"
#import "CountryPickerCell.h"


#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()<CustomNavigationBarDelegate,CLLocationManagerDelegate,UploadFileDelegate,UIAlertViewDelegate>
{
    NSArray * arrPracticenor;
    int practicenortype;
    NSMutableArray * arrCartype;
    NSMutableArray * arrSeat;
    NSString *pickerTitle;
    UIImage *resizedImage;
    NSDictionary * SignupResponse;
    NSArray *dropArray;
    NSString *countryCodeTaken;

}
@property (assign,nonatomic) BOOL isImageNeedsToUpload;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;
@property(nonatomic,strong)Cartype *selectedCarType;
@property(nonatomic,assign)NSInteger selectedCapacity;
@property(nonatomic,strong)UITextField *activeTextField;
@property(nonatomic,assign)BOOL isKeyboardIsShown;
@property(nonatomic,strong)SignUpDetails *signUpDetails;
@property(nonatomic,strong)UIImageView *profileImage;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeOutlet;


//-(PasswordStrengthType)checkPasswordStrength:(NSString *)password;
-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize helperCountry;
@synthesize helperCity;
@synthesize saveSignUpDetails;



#pragma mark -init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -view life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    self.title = NSLocalizedString(@"CREATE ACCOUNT", @"CREATE ACCOUNT");
    [self createNavLeftButton];
    [self createNavRightButton];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    [_countryCode setTitle:[NSString stringWithFormat:@"+%@",stringCountryCode] forState:UIControlStateNormal];
    _countryFlag.image = [UIImage imageNamed:imagePath];

    arrSeat = [[NSMutableArray alloc]initWithObjects:@"4",@"6",@"8", nil];
    isTnCButtonSelected = NO;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    self.termsLbl.titleLabel.text = NSLocalizedString(@"Agree to the terms and conditions", @"Agree to the terms and conditions");
}

-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];
    
    [navNextButton addTarget:self action:@selector(NextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    [Helper setButton:navNextButton Text:NSLocalizedString(@"NEXT", @"NEXT") WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)getCurrentLocation
{
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled])
    {
        if (!_locationManager)
        {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service", @"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
        [alertView show];
    }
}


#pragma mark -CLLocation manager deleagte method
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_6, __MAC_NA, __IPHONE_2_0, __IPHONE_6_0){
    
    _currentLatitude= newLocation.coordinate.latitude;
    _currentLongitude= newLocation.coordinate.longitude;
    [_locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}
- (IBAction)countryBtnCLicked:(id)sender
{
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString  *code, UIImage *flagimg, NSString *countryName)
    {
        self.countryFlag.image = flagimg;
        countryCodeTaken = [NSString stringWithFormat:@"+%@", code];
        [Helper setButton:self.countryCodeOutlet Text:countryCodeTaken WithFont:Robot_Light FSize:14 TitleColor:[UIColor blackColor] ShadowColor:nil];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}
#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender
{
    [self NextButtonClicked];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self cancelButtonClicked];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [_firstNameTF resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_emailTF   resignFirstResponder];
    [_passTF resignFirstResponder];
    [_phoneTF resignFirstResponder];
    [newSheet dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)signUp
{
    NSString *signupFirstName = _firstNameTF.text;
    NSString *lastName = _lastNameTF.text;
    NSString *signupEmail = _emailTF.text;
    NSString *signupPassword = _passTF.text;
    NSString *signupPhoneno = _phoneTF.text;
    NSString *selectCity = _selectCity.titleLabel.text;
    NSString *selectVehicle = _selectVehicle.titleLabel.text;
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter First Name", @"Enter First Name")];
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
        
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter contact number", @"Enter contact number")];
        
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
        
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
        
        
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select our Terms and Conditions", @"Please select our Terms and Conditions")];
    }
    else
    {
        
        NSString * pushToken;
        if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
        {
            pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
            
        }
        else
        {
            pushToken =@"dgfhfghr765998ghghj";
            
        }
        if (countryCodeTaken == nil)
        {
            countryCodeTaken = @"+91";
        }
        NSString * mobileno = [NSString stringWithFormat:@"%@%@",countryCodeTaken,self.phoneTF.text];
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing up", @"Signing up")];
        
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        
        if ([lastName length] == 0) {
            lastName = @"";
        }
        NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                     
                                     signupFirstName,kSMPSignUpFirstName,
                                     lastName,kSMPSignUpLastName,
                                     signupEmail,kSMPSignUpEmail,
                                     signupPassword, kSMPSignUpPassword,
                                     mobileno,kSMPSignUpMobile,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPSignUpDeviceId,
                                     pushToken,kSMPgetPushToken,
                                     @"1",kSMPSignUpDeviceType,
                                     [Helper getCurrentDateTime],kSMPSignUpDateTime,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"], @"ent_lang",
                                     selectCity,@"ent_city_name",
                                     selectVehicle,@"ent_services",
                                     nil];
        
        NSLog(@"param%@",queryParams);
        [handler composeRequestWithMethod:MethodPatientSignUp
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response)
         {
             if (succeeded) { //handle success response
                 [self signupResponse:response];
             }
             else{//error
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error") message:response[NSLocalizedString(@"error", @"error")] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
                 [alertView show];
                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
             }
         }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




-(void)signupResponse :(NSDictionary *)dictionary
{
    SignupResponse =dictionary;
    ProgressIndicator * progress = [ProgressIndicator sharedInstance];
    if ([dictionary[@"errFlag"] intValue] ==0) {
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:dictionary[@"data"][@"token"] forKey:KDAcheckUserSessionToken];
        [ud setObject:dictionary[@"data"][@"chn"] forKey:kNSURoadyoPubNubChannelkey];
        [ud setObject:dictionary[@"data"][@"email"] forKey:kNSUPatientEmailAddressKey];
        [ud setObject:@"1" forKey:KNSUIsFromSignUp];
        
        [_signUpDetails clear];
        
        if (_pickedImage != nil) {
            
            [self uploadImage];
        }
        else {
            [progress hideProgressIndicator];
            [self gotoHomeScreen];
        }
    }
    else{
        
        [progress hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictionary[@"errMsg"]];
    }
}
-(void)gotoHomeScreen{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:NSLocalizedString(@"Thankyou for your request.In next 24 hours one of our staff will contact you for further process.", @"Thankyou for your request.In next 24 hours one of our staff will contact you for further process.") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
    [alert show];
}

-(void)uploadImage{
    
    UploadFiles * upload = [[UploadFiles alloc]init];
    upload.delegate = self;
    [upload uploadImageFile:resizedImage];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==200) {
        
        [self cancelButtonClicked];
    }
    else {
        [self cancelButtonClicked];
    }
}

#pragma mark -ButtonAction


- (void)showDatePickerWithTitle:(UITextField*)textFeild
{
    pickerTitle = NSLocalizedString(@"Choose company", @"Choose company");
    newSheet=[[UIActionSheet alloc]initWithTitle:pickerTitle
                                        delegate:nil
                               cancelButtonTitle:nil
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil];
    newSheet.backgroundColor = [UIColor whiteColor];
    
    
    pickerview = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    pickerview.delegate = self;
    pickerview.dataSource = self;
    pickerview.showsSelectionIndicator = YES;
    pickerview.opaque = NO;
    pickerview.tag = 100;
    [self.view addSubview:pickerview];
    [newSheet addSubview:pickerview];
    UIButton *closeButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventTouchUpInside];
    
    [newSheet addSubview:closeButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7.0f, 70.0f, 30.0f);
    
    
    cancelButton.tintColor = [UIColor greenColor];
    [cancelButton addTarget:self action:@selector(dismissActionSheet1) forControlEvents:UIControlEventTouchUpInside];
    [newSheet addSubview:cancelButton];
    
    
    [newSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [newSheet setBounds:CGRectMake(0, 0, 320, 480)];
    
    
}

-(void)dismissActionSheet{
    
}

-(void)dismissActionSheet1{
    
}


#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    self.activeTextField = textField;
    
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _emailTF &&[_emailTF.text length] > 0 && ![Helper emailValidationCheck:_emailTF.text]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
        [_emailTF becomeFirstResponder];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange )range replacementString:(NSString *)string{
    if(textField==_phoneTF)
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        return (0 >=textField.text.length ||textField.text.length < 12);
    }
    else
    {
        return YES;
    }
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        [_lastNameTF becomeFirstResponder];
    }
    else if (textField == _lastNameTF) {
        [_phoneTF becomeFirstResponder];
    }
    else if (textField == _phoneTF) {
        [_emailTF becomeFirstResponder];
    }
    
    else if (textField == _emailTF) {
    }
    else if (textField == _passTF) {
        [self.view endEditing:YES];
    }
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        [_lastNameTF becomeFirstResponder];
    }
    else if (textField == _lastNameTF) {
        [_phoneTF becomeFirstResponder];
    }
    else if (textField == _phoneTF) {
        [_emailTF becomeFirstResponder];
    }
    
    else if (textField == _emailTF) {
        if (textField == _emailTF &&[_emailTF.text length] > 0 && ![Helper emailValidationCheck:_emailTF.text]) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
            [_emailTF becomeFirstResponder];
        }else{
            [_passTF becomeFirstResponder];
        }
        
    }
    else if(textField == _passTF)
    {
        int strength = [self checkPasswordStrength:_passTF.text];
        if(strength == 0)
        {
            [_passTF becomeFirstResponder];
        }else {
            [self.view endEditing:YES];
        }
    }
    return YES;
    
}
- (void)NextButtonClicked
{
    [self dismissKeyboard];
    [self signUp];
    
}


- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            case 2:
            {
                _profilePic.image = [UIImage imageNamed:@"aa_default_profile_pic.jpg"];
                _pickedImage = nil;
            }
            default:
                if (!_pickedImage) {
                    _profilePic.image = [UIImage imageNamed:@"aa_default_profile_pic.jpg"];
                }
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image Info : %@",info);
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    CGSize newSize = CGSizeMake(130.00f, 130.0f);
    UIGraphicsBeginImageContext(newSize);
    [_pickedImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSLog(@"pickedImage : %@",_pickedImage);
    
    //  [_signUpPhotoButton setImage:pickedImage forState:UIControlStateNormal];
    
    _profilePic.image = _pickedImage;
    
    
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self gotoHomeScreen];
    
    
    
    
}
-(void)uploadFile:(UploadFiles *)uploadfile didFailedWithError:(NSError *)error{
    NSLog(@"upload file  error %@",[error localizedDescription]);
    [self gotoHomeScreen];
}


#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter password first", @"Please enter password first")];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast one Uppercase alphabet", @"Please enter atleast one Uppercase alphabet")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast one Lowercase alphabet", @"Please enter atleast one Lowercase alphabet")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast  one Number", @"Please enter atleast  one Number")];
        return 0;
    }
    if (password.length < 5) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast  six Letters", @"Please enter atleast six Letters")];
        return 0;
    }
    return 1;
    
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

#pragma mark - UIKeyBoardNotification Methods
-(void) keyboardWillHide:(NSNotification *)note
{
    // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(64, 0, 0, 0);
        [[self tableView] setContentInset:edgeInsets];
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];
    
}
-(void) keyboardWillShow:(NSNotification *)note
{
    CGSize kbSize = CGSizeMake(320, 216);//[[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
                                         // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? kbSize.height : kbSize.width;
    
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = [[self tableView] contentInset];
        edgeInsets.bottom = height;
        [[self tableView] setContentInset:edgeInsets];
        edgeInsets = [[self tableView] scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];    _isKeyboardIsShown = YES;
}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    return cell;
}



#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
    
    
}


- (IBAction)selectImage:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (!_pickedImage)
    {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    }else{
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"),NSLocalizedString(@"Remove Photo", @"Remove Photo"), nil];
    }
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)termsNConditions:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)checkBoxAction:(id)sender {
    UIButton *mBut = (UIButton *)sender;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        [mBut setSelected:NO];
    }
    else
    {
        isTnCButtonSelected = YES;
        [mBut setSelected:YES];
        
    }
    
}


- (IBAction)selectVehicleAction:(id)sender {
}

- (IBAction)selectCityAction:(id)sender {
}
-(void)rel{
    //    [dropDown release];
}
- (IBAction)signup:(id)sender {
    [self.view endEditing:YES];
    [self signUp];
}
@end
