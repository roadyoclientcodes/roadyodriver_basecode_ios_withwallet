//
//  UpdateBookingStatus.m
//  Roadyo
//
//  Created by Surender Rathore on 07/08/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "UpdateBookingStatus.h"
#import "VNHPubNubWrapper.h"
#import "LocationTracker.m"
#import "LocationTracker.h"

@interface UpdateBookingStatus()
@property(nonatomic,strong)NSMutableDictionary *message;
@property LocationTracker * locationTracker;

@end
static UpdateBookingStatus *updateBookingStatus;

@implementation UpdateBookingStatus
@synthesize statusUpdateTimer;
@synthesize driverState;

+ (id)sharedInstance {
	if (!updateBookingStatus)
    {
		updateBookingStatus  = [[self alloc] init];
        [updateBookingStatus initlize];
	}
	return updateBookingStatus;
}
-(void)initlize{
    //_iterations = -1;
    _message = [[NSMutableDictionary alloc] init];
}
-(void)startUpdatingStatus{
    if(![statusUpdateTimer isValid]){
        statusUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateToPassengerForDriverState) userInfo:nil repeats:YES];
    }
}
-(void)stopUpdatingStatus
{
    if (_iterations < 0) {
        if ([statusUpdateTimer isValid])
        {
            [statusUpdateTimer invalidate];
        }
    }
}
-(void)updateToPassengerForDriverState{
    
    if (_iterations == 0) {
        
        // _message = nil;
        if ([statusUpdateTimer isValid]) {
            [statusUpdateTimer invalidate];
        }
        return;
    }
    
    _iterations --;
    
    NSLog(@"updateToPassengerForDriverState :%d",driverState);
    
    
    VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
    
    NSString *passengerChannel =[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPassengerChannelKey];
    
    NSString *bid = [[NSUserDefaults standardUserDefaults] objectForKey:@"bid"];
    if (bid == nil) {
        bid = @"0";
    }
    else
    {
        bid = bid;
    }
    NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSString *driverChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoSubscribeChanelKey];
    NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    NSString * pubNubChannel = [[NSUserDefaults standardUserDefaults]objectForKey:kNSURoadyoPubNubChannelkey];
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    
    
    [_message setObject:[NSNumber numberWithInt:driverState] forKey:@"a"];
    [_message setObject:driverEmail forKey:@"e_id"];
    [_message setObject:[NSNumber numberWithDouble:latitude] forKey:@"lt"];
    [_message setObject:[NSNumber numberWithDouble:longitude] forKey:@"lg"];
    [_message setObject:driverChannel forKey:@"chn"];
    [_message setObject:flStrForObj(dictP[@"dt"]) forKey:@"dt"];
    [_message setObject:bid forKey:@"bid"];
    if (driverState == kPubNubDriverCancelBooking) {
        [_message setObject:[NSNumber numberWithInt:_cancelBookingReason] forKey:@"r"];
    }
    if (driverState == kPubNubDriverBeginTrp) {
        NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        NSMutableArray *previousArray = [[ud objectForKey:@"saveLatLongs"] mutableCopy];
        if (previousArray.count)
        {
            [tempArray addObjectsFromArray:previousArray];
            [ud removeObjectForKey:@"saveLatLongs"];
        }
    }
    
    NSLog(@"_messageeeeeeee %@",_message);
    NSLog(@"passengerChannel...............%@",passengerChannel);
    [pubNub publishWithParameter:_message toChannel:flStrForStr(passengerChannel)];
    [pubNub publishWithParameter:_message toChannel:flStrForStr(pubNubChannel)];
}

-(void)updateToServerForBookingID:(NSInteger)bookingID
{
    NSString *serverChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey];
    
    NSMutableDictionary *message = [NSMutableDictionary new];
    
    [message setObject:@"11" forKey:@"a"];
    [message setObject:[Helper getCurrentDateTime] forKey:@"receiveDt"];
    [message setObject:[NSString stringWithFormat:@"%ld",(long)bookingID] forKey:@"bid"];
    
    NSLog(@"Booking Came Confirmation: %@",message);
    
    VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
    [pubNub publishWithParameter:message toChannel:serverChannel];
//    [pubNub sendMessageAsDictionary:message toChannel:serverChannel];
}
@end
