//
//  NewCalenderCell.h
//  Roadyo
//
//  Created by Rahul Sharma on 2/11/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCalenderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *pickUpImage;
@property (strong, nonatomic) IBOutlet UIImageView *dropOffImage;
@property (strong, nonatomic) IBOutlet UILabel *picLocLab;
@property (strong, nonatomic) IBOutlet UILabel *dropLocLab;

@property (strong, nonatomic) IBOutlet UILabel *bookingID;
@property (strong, nonatomic) IBOutlet UILabel *bookingTime;
@property (strong, nonatomic) IBOutlet UILabel *passengerName;
@property (strong, nonatomic) IBOutlet UILabel *pickupLocation;
@property (strong, nonatomic) IBOutlet UILabel *dropoffLocation;
@property (strong, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UILabel *totalDistance;
@property (strong, nonatomic) IBOutlet UILabel *totalJourneyTime;
@property (strong, nonatomic) IBOutlet UILabel *bIdLabl;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@end
